import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // this.btnClickedHandler = this.btnClickedHandler.bind(this);
    }

    btnClickedHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.btnClickedHandler}>Click here</button>
                <br />
                <p>You clicked {this.state.count} times</p>
            </div>
        )
    }
}

export default CountClick;